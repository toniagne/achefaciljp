<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'commands'], function(){

    Route::get('/artisan/{command}', function($command){

        try {
            \Illuminate\Support\Facades\Artisan::call($command);
        } catch(\Exception $e){
            dd($e->getMessage());
        }

        dd(\Illuminate\Support\Facades\Artisan::output());
    });


    Route::get('terminal/{command}', function($command) {
        $cli = new \Symfony\Component\Process\Process(['composer' => 'install']);
        $cli->run();

        dd($cli->getOutput());
    });

});


Route::get('/', 'SiteController@index')->name('home');
Route::post('search', 'HomeController@search')->name('search');
Route::get('cliente/{id}', 'HomeController@client')->name('home.client');
Route::get('voucher/{id}', 'HomeController@voucher')->name('home.voucher');
Route::get('categoria/{slug}', 'HomeController@category')->name('home.category');

Auth::routes(['verify' => true]);

Route::get('/{vue_capture?}', function () {
    return view('home');
})->where('vue_capture', '[\/\w\.-]*')->middleware('auth');
