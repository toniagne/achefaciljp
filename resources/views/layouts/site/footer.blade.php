<footer id="footer">
    <div class="container">


        <div class="col-sm-3">

            <div class="widget widget-list">
                <h5>Links</h5>
                <ul class="footer">



                    <li><a href="pg/1/quem-somos/index.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Quem Somos</a></li>
                    <li><a href="pg/2/termos-e-condicoes/index.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Termos e Condições</a></li>
                    <li><a href="anuncie-aqui.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;ANUNCIE NO SITE</a></li>
                    <li><a href="fale-conosco.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;FALE CONOSCO</a></li>
                </ul>
            </div><!-- /.widget-list -->
        </div>
        <div class="col-sm-3">

            <div class="widget widget-list">
                <h5>Serviços</h5>
                <ul class="footer">



                    <li><a href="pg/3/telefones-uteis/index.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Telefones Úteis</a></li>
                    <li><a href="pg/8/teatros/index.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Teatros</a></li>
                    <li><a href="pg/9/cinemas/index.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Cinemas</a></li>
                </ul>
            </div><!-- /.widget-list -->
        </div>
        <div class="col-sm-3">

            <div class="widget widget-list">
                <h5>Turismo</h5>
                <ul class="footer">



                    <li><a href="pg/4/conheca-a-cidade/index.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Conheça a Cidade</a></li>
                    <li><a href="pg/6/como-chegar/index.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Como Chegar</a></li>
                    <li><a href="pg/5/pontos-turisticos/index.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Pontos Turísticos</a></li>
                    <li><a href="pg/7/utilidade-publica/index.html"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;Utilidade Pública</a></li>
                </ul>
            </div><!-- /.widget-list -->
        </div>

        <script type="text/javascript">
            $(window).load(function(){
                $('#facebook_like_box')
                    .html('<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fuol&amp;width=260&amp;height=240&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:240px; margin-top:15px;" allowTransparency="true"></iframe>');
            });
        </script>
        <div class="col-sm-3">
            <div id="facebook_like_box"></div>
        </div>
    </div>
    <div class="btmFooter">
        <div class="container">
            <div class="col-sm-6 mTop-10">               <p>
                    Todos os direitos reservados - Permitida a reprodução do conteúdo deste portal desde que autorizada.
                </p>
            </div>
            <div class="col-md-4 sociais mTop-10">

                <a href="https://api.whatsapp.com/send?1=pt_BR&amp;phone=550(00)%207777-8888"  data-toggle="tooltip" title="(00) 7777-8888"><i class="fa fa-whatsapp"></i></a>           		<a href="https://www.facebook.com/#" data-toggle="tooltip" title="Sistema de Guia Comercial com aplicativo Internet media - Tenha seu Guia comercial no Facebook" target="_blank"><i class="fa fa-facebook-square"></i></a>				<a href="https://www.twitter.com/" data-toggle="tooltip" title="Sistema de Guia Comercial com aplicativo Internet media - Tenha seu Guia comercial no Twitter" target="_blank"><i class="fa fa-twitter-square"></i></a>				<a href="https://www.youtube.com/" data-toggle="tooltip" title="Sistema de Guia Comercial com aplicativo Internet media - Tenha seu Guia comercial no YouTube" target="_blank"><i class="fa fa-youtube-square"></i></a>                <a href="https://www.google.com.br/" data-toggle="tooltip" title="Sistema de Guia Comercial com aplicativo Internet media - Tenha seu Guia comercial no Google +" target="_blank"><i class="fa fa-google-plus-square"></i></a>                <a href="https://www.instagram.com/" data-toggle="tooltip" title="Sistema de Guia Comercial com aplicativo Internet media - Tenha seu Guia comercial no Instagram" target="_blank"><i class="fa fa-instagram"></i></a>
            </div>
            <div class="col-sm-1 mTop-10"> <a href="https://www.internetmedia.com.br/" target="_blank"><img src="template/imagem/desenvolvedor.png"/></a> </div>          </div>
    </div>
</footer>
