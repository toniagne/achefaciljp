<nav id="mp-menu" class="mp-menu">
    <div class="mp-level">
        <h2>MENU</h2>
        <ul>
            <li><a href="#">Classificados <strong class="caret"></strong></a>
                <ul class="sub-menu lato slim">           
                    <li><a href="imoveis/index.html#toop"><i class="material-icons vermelho 10">business</i>&nbsp;&nbsp;&nbsp; imóveis</a></li>
                    <li><a href="empregos/index.html#toop"><i class="material-icons vermelho 10">card_travel</i>&nbsp;&nbsp;&nbsp; empregos</a></li>
                </ul><!-- /.submenu -->
            </li>
            <li><a href="anuncie-aqui.html#toop">Anuncie</a></li> 
    
            <li><i class="fa fa-map-marker cor"></i> <span style="font-weight:bold;">&nbsp; </span></li> 
        </ul>
    </div>
</nav>
