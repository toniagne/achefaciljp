@include('layouts.site.header')
<body id="top">
<div class="mp-pusher" id="mp-pusher">

    <!-- /mp-menu -->
    <div class="scroller">
        <div class="scroller-inner">
            <header>

                <div id="menu-topo">
                    <div class="container">
                        <div class="row">
                            <div class='col-md-9'>
                                <nav>
                                    <ul class="menu">


                                        <li><a href="{{ route('home') }}"><i class="fa fa-home fonte15 cor"></i></a></li>


                                        <li><a href="#">Classificados <strong class="caret"></strong></a>
                                            <ul class="sub-menu lato slim">

                                                <li><a href="imoveis/index.html#toop"><i class="material-icons vermelho 10">business</i>&nbsp;&nbsp;&nbsp; imóveis</a></li>
                                                <li><a href="automoveis/index.html#toop"><i class="material-icons vermelho 10">drive_eta</i>&nbsp;&nbsp;&nbsp; automóveis</a></li>
                                                <li><a href="empregos/index.html#toop"><i class="material-icons vermelho 10">card_travel</i>&nbsp;&nbsp;&nbsp; empregos</a></li>
                                                <li><a href="diversos/index.html#toop"><i class="material-icons vermelho 10">map</i>&nbsp;&nbsp;&nbsp; diversos</a></li>
                                            </ul><!-- /.submenu -->
                                        </li>
                                        <li><a href="anuncie-aqui.html#toop">Anuncie</a></li>


                                    </ul>
                                </nav>
                            </div>
                            <div class="col-md-3 ">             <li>
                                    <a data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();" style="border:none"><span data-toggle="tooltip" data-placement="bottom" title="Clique para Logar"><i class="fa fa-user fonte15 cor"></i> Fale conosco </span></a>
                                </li>
                            </div>




                        </div>
                    </div>
                </div>


                <div class="container exibir">
                    <div class="row">
                        <div class="top-wrap">
                            <div id="menu-mobile">
                                <a href="#" id="trigger"><i class="fa fa-bars shortcut-icon branco"></i></a>
                            </div>
                            <div id="logo-resp">
                                <a href="{{ route('home') }}">  <img src="{{asset('img/logo-af.png')}}" /></a>
                            </div>



                            <div class="mcidade">
                                <a href="#" class="popup-cidade"><i class="fa fa-map-marker"></i></a>
                            </div>

                            <div id="sb-search" class="sb-search">
                                <form  action="{{ route('search') }}" method="post">
                                    <input type="hidden" value="emp" name="y" />
                                    <input class="sb-search-input" placeholder="Buscar em ..." type="text" name="key" id="searchss">

                                    <input class="sb-search-submit" type="submit" value="">
                                    <span class="sb-icon-search"><i class="fa fa-search"></i></span>
                                </form>
                            </div>

                        </div>




                        <div id="topinho">
                            <ul>
                                <li><a class="btn" href="#"><i class="fa fa-home"></i>&nbsp;&nbsp;HOME</a></li>                         <li><a class="btn" href="guia-categorias.html"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;CATEGORIAS</a></li>
                                <li><a class="btn" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();"><i class="fa fa-sign-in"></i>&nbsp;&nbsp;LOGIN</a></li>

                            </ul>
                        </div>


                    </div><!-- /.row -->
                </div><!-- /.container -->

                <!-- FIM TOPO RESPONSIVO -->
                <!--SE FOR PC-->
                <div class="header_inner ocultar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4 ocultar">
                                <a href="{{ route('home') }}">
                                    <img src="{{ asset('img/logo-af.png') }}" height="50" />
                                </a>
                            </div>
                            <div class="col-sm-8 banner" align="right">

                                <div class="row busca-search-box-topo">
                                    <form  action="https://www.achefacil.com.br/buscar/#toop" method="post">
                                        <div class="row col-sm-10">
                                            <input type="text" class="form-control2 input-sm search-first" placeholder="Ex: Restaurante" name="key" style="font-size:20px" required>

                                        </div>

                                        <div class="col-sm-2 search-input">
                                            <button class="btn btn-danger btn-sm btn-raised ripple-effect" style="width:100%"><i class="fa fa-search"></i></button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </header>

        @yield('content')

        <!-- modal login -->
        @include('layouts.modals.contato')
        <!-- /modal login -->

            <a href="#0" class="cd-top">topo</a>
        </div><!-- /scroller-inner -->
    </div><!-- /scroller -->

    <div class="anuncie-box clearfix ocultar">
        <div class="container">
            <div class="row">
                <div class="col-md-4 pull-right">
                    <a href="anuncie-aqui.html" class="btn btn-raised btn-danger ripple-effect btn-lg" data-original-title="" title="">
                        <i class="fa fa-bullhorn" aria-hidden="true"></i> &nbsp; CADASTRE SUA EMPRESA
                    </a>
                </div>
                <div class="col-md-8">
                    <h2>
                        Seja visto por centenas de pessoas diariamente
                    </h2>
                    <p>
                        Cadastre-se agora mesmo em nosso guia comercial, conheça agora mesmo nossos planos !
                    </p>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.site.footer')
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#myModal").modal('show');
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".popup-cidade").click(function(){
            $("#myModal").modal('show');
        });
    });
</script>
<!-- Javascript -->
<script src="template/js/classie.js"></script>
<script src="template/js/mlpushmenu.js"></script>
<script>
    new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );
</script>
<script src="template/js/uisearch.js"></script>
<script>
    new UISearch( document.getElementById( 'sb-search' ) );
</script>
<script src="owlcarousel/owl.carousel.js"></script>
<script src="template/js/main.js"></script>
<script src="template/js/jquery-ui.js"></script>
<!--<script src="https://www.achefacil.com.br/template/js/jquery.maskedinput.js"></script>-->
<script src="template/js/bootstrap.min.js"></script>
<script src="template/js/jquery.animsition.min.js"></script>
<script src="template/js/plugins.js"></script>
<div id="menuHeader" class="ocultar">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="row col-sm-4" style="margin-top:20px">
                    <a href="index.html">
                        <img src="{{ asset('img/logo-af.png') }}" />
                    </a></div>
                <div class="row busca-search-box-topo col-sm-8">
                    <form  action="https://www.achefacil.com.br/buscar/" method="post">
                        <div class="row col-sm-10">
                            <input type="text" class="form-control2 input-lga search-first" placeholder="O que você deseja encontrar?" name="key" style="font-size:20px" required>
                            <div style=" position:absolute; right:40px; top:15px; font-size:26px"><a href="guia-categorias.html" data-placement="bottom"  data-toggle="tooltip" title="TODAS AS CATEGORIAS"><i class="fa fa-bars shortcut-icon cinza"></i></a></div>

                        </div>

                        <div class="col-sm-2 search-input">
                            <button class="btn btn-danger btn-lg btn-raised ripple-effect" style="width:100%"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>


            </div>
        </div>

    </div>
</div>
<!-- /.busca oc -->




</body>

</html>
