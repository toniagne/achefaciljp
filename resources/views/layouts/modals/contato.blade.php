<div class="modal fade login" id="loginModal" align="center">
                <div class="modal-dialog login animated">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Login</h4>
                        </div>
                        <div class="modal-body col-md-12">
                            <div class="box" style="width:100%">
                                <div class="content">

                                    <div class="form loginBox">

                                        <div class="social">

                                            <a href="facebook/index.html">
                                                <button class="btn btn-primary btn-raised btn-lg ripple-effect" style="background:#3b5998"><i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;&nbsp; LOGAR COM FACEBOOK</button></a>
                                        </div>
                                        <div class="division">
                                            <div class="line l"></div>
                                            <span>ou</span>
                                            <div class="line r"></div>
                                        </div>


                                        <form  action="https://www.achefacil.com.br/logar" id="signupform" name="signupform" method="post">
                                            <input type='hidden' name='logar' value='5'  />
                                            <input type='hidden' name='urlDir' value='' />

                                            Usuário: demo@demo.com<br>
                                            <input name="login_login" type="email" required class="form-control" id="login_login" placeholder="Seu email" value="demo@demo.com">
                                            Senha: demo1234<br>
                                            <input name="senha_login" type="password" required class="form-control" id="senha_login" placeholder="Senha" value="demo1234">
                                            <div align="center">
                                                <input class="btn btn-danger btn-lg btn-raised ripple-effect" type="submit" value="Entrar">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="box" style="width:100%">
                                <div class="content registerBox" style="display:none;">
                                    <div class="form">
                                        <script>
                                            $(document).ready(function veremail(){
                                                $("input[name='email']").blur( function(){
                                                    var email = $("input[name='email']").val();
                                                    $.post('ajax/consultaemail.html',{email: email},function(data){
                                                        $('#resultadoemail').html(data);
                                                    });

                                                });
                                            });
                                        </script>

                                        <form id="signupform" name="signupform" method="post" action="https://www.achefacil.com.br/cadastra-visitante">
                                            <input type='hidden' name='franquia' value=''  />

                                            <input class="form-control" type="text" placeholder="Nome Completo" name="nome" required>

                                            <input id="email" class="form-control" type="email" placeholder="Email" name="email" onblur="veremail()" required>
                                            <div id="resultadoemail"></div>
                                            <input id="password" class="form-control" type="password" placeholder="Defina uma senha" name="senha" required>

                                            <div align="center"><input class="btn btn-danger btn-lg btn-raised ripple-effect" type="submit" value="CADASTRAR" name="commit"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="forgot login-footer">
                            <span>Não é cadastrado?
                                 <a href="javascript: showRegisterForm();">Cadastre-se</a>
                            ?</span>
                            </div>
                            <div class="forgot register-footer" style="display:none">
                                <span>Já possui uma conta?</span>
                                <a href="javascript: showLoginForm();">Efetue o Login</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>