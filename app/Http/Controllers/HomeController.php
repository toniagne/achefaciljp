<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{



    public function index()
    {
        return view('home');
    }

    public function client($id)
    {
        return view('site.cliente');
    }

    public function voucher($id)
    {

    }

    public function category($slug)
    {
        return view('site.category-list');
    }

}
