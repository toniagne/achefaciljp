<?php echo $__env->make('layouts.site.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<body id="top">
<div class="mp-pusher" id="mp-pusher">
   
    <!-- /mp-menu -->
    <div class="scroller">
        <div class="scroller-inner">
            <header>

                <div id="menu-topo">
                    <div class="container">
                        <div class="row">
                            <div class='col-md-9'>
                                <nav>
                                    <ul class="menu">


                                        <li><a href="<?php echo e(route('home')); ?>"><i class="fa fa-home fonte15 cor"></i></a></li>
                                        

                                        <li><a href="#">Classificados <strong class="caret"></strong></a>
                                            <ul class="sub-menu lato slim">

                                                <li><a href="imoveis/index.html#toop"><i class="material-icons vermelho 10">business</i>&nbsp;&nbsp;&nbsp; imóveis</a></li>
                                                <li><a href="automoveis/index.html#toop"><i class="material-icons vermelho 10">drive_eta</i>&nbsp;&nbsp;&nbsp; automóveis</a></li>
                                                <li><a href="empregos/index.html#toop"><i class="material-icons vermelho 10">card_travel</i>&nbsp;&nbsp;&nbsp; empregos</a></li>
                                                <li><a href="diversos/index.html#toop"><i class="material-icons vermelho 10">map</i>&nbsp;&nbsp;&nbsp; diversos</a></li>
                                            </ul><!-- /.submenu -->
                                        </li>
                                         <li><a href="anuncie-aqui.html#toop">Anuncie</a></li>

                                       
                                        </ul>
                                </nav>
                            </div>
                            <div class="col-md-3 ">             <li>
                                    <a data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();" style="border:none"><span data-toggle="tooltip" data-placement="bottom" title="Clique para Logar"><i class="fa fa-user fonte15 cor"></i> Fale conosco </span></a>
                                </li>
                            </div>


                           

                        </div>
                    </div>
                </div>


                <div class="container exibir">
                    <div class="row">
                        <div class="top-wrap">
                            <div id="menu-mobile">
                                <a href="#" id="trigger"><i class="fa fa-bars shortcut-icon branco"></i></a>
                            </div>
                            <div id="logo-resp">
                                <a href="<?php echo e(route('home')); ?>">  <img src="<?php echo e(asset('img/logo-af.png')); ?>" /></a>
                            </div>



                            <div class="mcidade">
                                <a href="#" class="popup-cidade"><i class="fa fa-map-marker"></i></a>
                            </div>

                            <div id="sb-search" class="sb-search">
                                <form  action="<?php echo e(route('search')); ?>" method="post">
                                    <input type="hidden" value="emp" name="y" />
                                    <input class="sb-search-input" placeholder="Buscar em ..." type="text" name="key" id="searchss">

                                    <input class="sb-search-submit" type="submit" value="">
                                    <span class="sb-icon-search"><i class="fa fa-search"></i></span>
                                </form>
                            </div>

                        </div>




                        <div id="topinho">
                            <ul>
                                <li><a class="btn" href="#"><i class="fa fa-home"></i>&nbsp;&nbsp;HOME</a></li>                         <li><a class="btn" href="guia-categorias.html"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;CATEGORIAS</a></li>
                                <li><a class="btn" data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();"><i class="fa fa-sign-in"></i>&nbsp;&nbsp;LOGIN</a></li>

                            </ul>
                        </div>


                    </div><!-- /.row -->
                </div><!-- /.container -->

                <!-- FIM TOPO RESPONSIVO -->
                <!--SE FOR PC-->
                <div class="header ocultar">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4 ocultar">
                            <a href="<?php echo e(route('home')); ?>">
                                    <img src="<?php echo e(asset('img/logo-af.png')); ?>" />
                                </a>
                            </div>
                            <div class="col-sm-8 banner" align="right">
                                <img src="<?php echo e(asset('img/banner_728x90.jpg')); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </header>


            <div class="busca ocultar">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <br> <div class="row text-center">

                                <h2>O que você deseja encontrar?</h2>
                                <p>Tudo o que precisa em um só lugar. Seja bem vindo nosso guia comercial!</p>
                            </div>
                            <div class="row busca-search-box">
                                <form  action="https://www.achefacil.com.br/buscar/#toop" method="post">
                                    <div class="row col-sm-10">
                                        <input type="text" class="form-control2 input-lga search-first" placeholder="Ex: Restaurante" name="key" style="font-size:20px" required>
                                        <div id="menu-categorias"><a href="guia-categorias.html#toop"  data-ctoggle="tooltip" title="TODAS AS CATEGORIAS"><i class="fa fa-bars shortcut-icon cinza"></i></a></div>

                                    </div>
                                    <!--<div class="col-md-4 col-sm-4 search-input">
                                                <select class="form-control input-lg search-second">
                                                    <option selected="">Todos os lugares</option>
                                                    <option>Rio de Janeiro</option>
                                                    <option>São Paulo</option>
                                                    <option>Campo Grande</option>
                                                </select>
                                    </div>-->
                                    <div class="col-sm-2 search-input">


                                        <button class="btn btn-danger btn-lg btn-raised ripple-effect" style="width:100%"><i class="fa fa-search"></i></button>
                                    </div>
                                </form>
                            </div>


                        </div>
                    </div>
                    <div class="frameLR" style="margin-top:40px;">
                        <div class="row">
                            <div  style=" width:100%; text-align:center !important">
                                <ul class="nav nav-pills text-center" role="tablist">
                                    <li>
                                        <a href="<?php echo e(route('home.category', 'restaurantes')); ?>">
                                            <i class="material-icons">restaurant</i>
                                            Restaurante
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('home.category', 'delivery')); ?>">
                                            <i class="material-icons">local_phone</i>
                                            Delivery</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('home.category', 'pizzaria')); ?>">
                                            <i class="material-icons">local_pizza</i>
                                            Pizzaria
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo e(route('home.category', 'taxi')); ?>">
                                            <i class="material-icons">local_taxi</i>
                                            Taxi
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?php echo e(route('home.category', 'farmacias')); ?>l">
                                            <i class="material-icons">enhanced_encryption</i>
                                            Farmácia
                                        </a>
                                    </li>


                                    <li>
                                        <a href="<?php echo e(route('home.category', 'academias')); ?>">
                                            <i class="material-icons">directions_bike</i>
                                            Academias
                                        </a>
                                    </li>


                                </ul>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.fIM TOPO BUSCAR HOME -->

           <?php echo $__env->yieldContent('content'); ?>

            <!-- modal login -->
            <?php echo $__env->make('layouts.modals.contato', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- /modal login -->
            
            <a href="#0" class="cd-top">topo</a>
        </div><!-- /scroller-inner -->
    </div><!-- /scroller -->

    <div class="anuncie-box clearfix ocultar">
        <div class="container">
            <div class="row">
                <div class="col-md-4 pull-right">
                    <a href="anuncie-aqui.html" class="btn btn-raised btn-danger ripple-effect btn-lg" data-original-title="" title="">
                        <i class="fa fa-bullhorn" aria-hidden="true"></i> &nbsp; CADASTRE SUA EMPRESA
                    </a>
                </div>
                <div class="col-md-8">
                    <h2>
                        Seja visto por centenas de pessoas diariamente
                    </h2>
                    <p>
                        Cadastre-se agora mesmo em nosso guia comercial, conheça agora mesmo nossos planos !
                    </p>
                </div>
            </div>
        </div>
    </div>

    <?php echo $__env->make('layouts.site.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#myModal").modal('show');
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".popup-cidade").click(function(){
            $("#myModal").modal('show');
        });
    });
</script>
<!-- Javascript -->
<script src="template/js/classie.js"></script>
<script src="template/js/mlpushmenu.js"></script>
<script>
    new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );
</script>
<script src="template/js/uisearch.js"></script>
<script>
    new UISearch( document.getElementById( 'sb-search' ) );
</script>
<script src="owlcarousel/owl.carousel.js"></script>
<script src="template/js/main.js"></script>
<script src="template/js/jquery-ui.js"></script>
<!--<script src="https://www.achefacil.com.br/template/js/jquery.maskedinput.js"></script>-->
<script src="template/js/bootstrap.min.js"></script>
<script src="template/js/jquery.animsition.min.js"></script>
<script src="template/js/plugins.js"></script>
<div id="menuHeader" class="ocultar">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="row col-sm-4" style="margin-top:20px">
                    <a href="index.html">
                        <img src="<?php echo e(asset('img/logo-af.png')); ?>" />
                    </a></div>
                <div class="row busca-search-box-topo col-sm-8">
                    <form  action="https://www.achefacil.com.br/buscar/" method="post">
                        <div class="row col-sm-10">
                            <input type="text" class="form-control2 input-lga search-first" placeholder="O que você deseja encontrar?" name="key" style="font-size:20px" required>
                            <div style=" position:absolute; right:40px; top:15px; font-size:26px"><a href="guia-categorias.html" data-placement="bottom"  data-toggle="tooltip" title="TODAS AS CATEGORIAS"><i class="fa fa-bars shortcut-icon cinza"></i></a></div>

                        </div>

                        <div class="col-sm-2 search-input">
                            <button class="btn btn-danger btn-lg btn-raised ripple-effect" style="width:100%"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>


            </div>
        </div>

    </div>
</div>
<!-- /.busca oc -->

 


</body>

</html>
<?php /**PATH /home/storage/b/dc/a5/achefaciljp1/public_html/resources/views/layouts/site/master.blade.php ENDPATH**/ ?>