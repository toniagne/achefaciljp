<!DOCTYPE html>
<html lang="pt-br" class="no-js">

<meta http-equiv="content-type" content="text/html;charset=iso-utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="theme-color" content="#3d4687"/>
    <meta charset="utf-8">
    <meta name="language" content="pt-br">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>AcheFácilJP | <?php echo $__env->yieldContent('title'); ?></title>
    <meta name="description" content="AcheFácilJP - Guia comercial de Cachoeira do Sul" />
    <meta name="keywords" content="sistema guia comercial, aplicativo guia comercial, guia comercial php, criar guia comercial" />
    <meta property="og:site_name" content="AcheFácilJP - Guia comercial de Cachoeira do Sul" />
    <meta property="og:type" content="article" />
    <meta content="https://www.achefacil.com.br" property="og:url">
    <meta content="https://www.achefacil.com.br/arquivos/template/1eece06cde.jpg" property="og:image">
    <meta content="AcheFácilJP - Guia comercial de Cachoeira do Sul" property="og:title">
    <meta content="AcheFácilJP - Guia comercial de Cachoeira do Sul" property="og:description">
    <link href="<?php echo e(asset("arquivos/template/1eece06cde.jpg")); ?>" rel="image_src" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="400">
    <meta property="og:image:height" content="400">
    <link href="<?php echo e(asset("template/css/bootstrap.min.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(asset("owlcarousel/assets/owl.carousel.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(asset("owlcarousel/assets/owl.theme.default.min.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(asset("template/css/animate.min.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(asset("template/css/animsition.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(asset("template/css/gridGallery.css")); ?>" rel="stylesheet" type="text/css" >
    <link href="<?php echo e(asset("template/css/plugins.min.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(asset("uploadify/uploadify.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(asset("template/css/material-kit.css")); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset("template/css/component.css")); ?>" rel="stylesheet" media="screen">
    <link href="<?php echo e(asset("css/style.css")); ?>" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="arquivos/template/1404168ad1.gif" type="image/x-icon">
    <link rel="icon" href="arquivos/template/1404168ad1.gif" type="image/x-icon">
    <script src="<?php echo e(asset("template/js/modernizr.custom.js")); ?>"></script>
    <script src="<?php echo e(asset("template/js/jquery.min.js")); ?>"></script>
    <script src="<?php echo e(asset("template/js/menu.js")); ?>"></script>
    <script async src="<?php echo e(asset("template/js/jquery.flexslider.js")); ?>"></script>
    <script src="<?php echo e(asset("template/js/material.min.js")); ?>"></script>
    <script src="<?php echo e(asset("template/js/material-kit.js")); ?>"></script>
    <script async src="<?php echo e(asset("template/js/jquery.maskedinput.js")); ?>"></script>
  

    
</head>
<?php /**PATH /home/storage/b/dc/a5/achefaciljp1/public_html/resources/views/layouts/site/header.blade.php ENDPATH**/ ?>