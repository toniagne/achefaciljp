<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <li class="nav-item">
        <router-link to="/dashboard" class="nav-link">
          <i class="nav-icon fas fa-tachometer-alt blue"></i>
          <p>
            Área inicial
          </p>
        </router-link>
      </li>

      <li class="nav-item">
        <router-link to="/contracts" class="nav-link">
          <i class="nav-icon fas fa-book orange"></i>
          <p>
            Contratos
          </p>
        </router-link>
      </li>

        <li class="nav-item">
            <router-link to="/clients" class="nav-link">
                <i class="nav-icon fas fa-gem orange"></i>
                <p>
                    Clientes
                </p>
            </router-link>
        </li>

        <li class="nav-item">
            <router-link to="/banners" class="nav-link">
                <i class="nav-icon fas fa-shopping-bag orange"></i>
                <p>
                    Banners
                </p>
            </router-link>
        </li>

      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAdmin')): ?>
        <li class="nav-item">
          <router-link to="/users" class="nav-link">
            <i class="fa fa-users nav-icon blue"></i>
            <p>Administradores</p>
          </router-link>
        </li>
      <?php endif; ?>



      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isAdmin')): ?>
      <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-cog green"></i>
          <p>
            Configurações
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">

          <li class="nav-item">
            <router-link to="/product/category" class="nav-link">
              <i class="nav-icon fas fa-list-ol green"></i>
              <p>
                Categorias
              </p>
            </router-link>
          </li>
          <li class="nav-item">
            <router-link to="/product/tag" class="nav-link">
              <i class="nav-icon fas fa-tags green"></i>
              <p>
                Subcategorias
              </p>
            </router-link>
          </li>

            <li class="nav-item">
              <router-link to="/developer" class="nav-link">
                  <i class="nav-icon fas fa-cogs white"></i>
                  <p>
                      Developer
                  </p>
              </router-link>
            </li>
        </ul>
      </li>

      <?php endif; ?>



      <li class="nav-item">
        <a href="#" class="nav-link" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="nav-icon fas fa-power-off red"></i>
          <p>
            Sair
          </p>
        </a>
        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
          <?php echo csrf_field(); ?>
        </form>
      </li>
    </ul>
  </nav>
<?php /**PATH /home/storage/b/dc/a5/achefaciljp1/public_html/resources/views/layouts/sidebar-menu.blade.php ENDPATH**/ ?>