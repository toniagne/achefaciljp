

<?php $__env->startSection('title'); ?> Seja Bem vindo <?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<section id="page" class="container">

 <script>
    $(document).ready(function() {
    
	$('.banners').owlCarousel({
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
	loop:true,
    margin:10,
    nav:false,
    responsive:{
        0:{
            items:3
        },
        600:{
            items:3
        },
        1000:{
            items:5        }
    }
})

 });
    </script>	

<div class="row animated fadeIn">
    <Br>
    <div class="col-sm-12 clearfix mt-5">   
        <div class="banners">
                <div class="item">
                    <a href="<?php echo e(route('home.client', 1)); ?>">
                    <img src="<?php echo e(asset('img/zerograu_300x200.jpg')); ?>" alt="PARCEIROS">
                    </a>
                </div>

                <div class="item">
                    <a href="<?php echo e(route('home.client', 1)); ?>">
                    <img src="<?php echo e(asset('img/erefrilar_300x200.jpg')); ?>" alt="PARCEIROS">
                    </a>
                </div>
        </div>
    </div>
</div>
<!--fim publicidades-->           
 


<div class="gn-line"></div>
        <div class="no-class">
            <div class="row">
              <div class="col-sm-12 clearfix">
                
                <div class="titulos">
                  <h2>PROMOÇÕES</h2>
                  <div class="linhap vermelho"></div>
                  <div class="linha"></div>
                  <div class="ver-todos">
                    <a href="<?php echo e('home.voucher', 1); ?>">
                      VER TODOS
                    </a>
                  </div>
                </div>
                
                
                
                
              </div>
            </div>
            
<div class="row mTop-20">                 
<div class="cupons">
    
    
    
     
     
     <div class="item">
       <div class="col-sm-3">
                <div class="cupom" style="min-width:260px;">
                  <div class="desconto">
                  <button class="btn btn-raised btn-fab btn-round" id="bgvermelho" >- 30%</button>
                  </div>
                  <a href="<?php echo e(route('home.client', 1)); ?>">
                  <div class="imagem" style="background:url('<?php echo e(asset('img/casa-frango-desconto.jpg')); ?>') center center/cover; min-height:170px; max-height:170px;">
                  </div> </a>
                  <!-- /.imagem -->
                  <div class="titulo">
                    <a>Casa do frango D'Itália                    </a>
                  <p>(1) Frango Assado com polenta</p>  
                  </div>
               </div>
            </div>
              <!-- /.cupom -->
          </div>
    </div>
          
</div>
</div>
</div>
<!-- / CUPOM DESCONTO -->
 
<!--BANNERS -->
            
<div class="no-class row">      
<div class="col-sm-4 mTop-30"> 
			
<div class="row widget">
<div class="col-md-12">
<div class="flexslider">
<ul class="slides">
<li>
<img src='img/834a23f88966b238ac73c02c9577e75f.gif' border='0' class="img-right">
</li>
</ul>
</div>
</div>
</div>
            </div>
<div class="col-sm-4 mTop-30">

<div class="row widget">
<div class="col-md-12">
<div class="flexslider">
<ul class="slides">
<li>
<img src='img/9c37f23c61dd0d9c39a324ec64be5902.gif' border='0' class="img-right">
</li>
</ul>
</div>
</div>
</div>
            </div>
<div class="col-sm-4 mTop-30">
<div class="row widget">
<div class="col-md-12">
<div class="flexslider">
<ul class="slides">
<li>
<a href='http://www.internetmedia.com.br/' target='_blank'><img src='img/151c2853b9bf28774e6eb831d7390e38.gif' border='0' class="img-right"></a></li>
</ul>
</div>
</div>
</div>
            </div>       
</div>

          
</div>
<!--/.DESTAQUES --> 


</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.site.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/storage/b/dc/a5/achefaciljp1/public_html/resources/views/site/index.blade.php ENDPATH**/ ?>